package com.debleserp.demo.webvaadin;

import com.debleserp.demo.entity.Animal;
import com.debleserp.demo.entity.AnimalCaretaker;
import com.debleserp.demo.entity.Zoo;
import com.debleserp.demo.repository.AnimalCaretakerRepository;
import com.debleserp.demo.repository.AnimalRepository;
import com.debleserp.demo.repository.ZooRepository;
import com.debleserp.demo.webvaadin.components.AnimalCaretakerEditor;
import com.debleserp.demo.webvaadin.components.AnimalEditor;
import com.debleserp.demo.webvaadin.components.ZooEditor;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.Route;
import org.apache.commons.lang3.StringUtils;

@Route(value = "")
public class MainView extends VerticalLayout {


    private final AnimalRepository animalRepository;
    private final AnimalCaretakerRepository animalCaretakerRepository;
    private final ZooRepository zooRepository;

    final Label titleLabel = new Label();
    final HorizontalLayout horizontalLayout = new HorizontalLayout();

    public MainView(AnimalRepository animalRepository, AnimalCaretakerRepository animalCaretakerRepository, ZooRepository zooRepository) {

        this.animalRepository = animalRepository;
        this.animalCaretakerRepository = animalCaretakerRepository;
        this.zooRepository = zooRepository;
        //Generate menu, will not be added to view yet
        Button buttonWelcome = new Button("Home", event -> loadHomeView());
        horizontalLayout.add(buttonWelcome);
        Button buttonCaretakers = new Button("Caretakers",event -> loadCaretakerView());
        horizontalLayout.add(buttonCaretakers);
        Button buttonAnimals = new Button("Animals", event -> loadAnimalView());
        horizontalLayout.add(buttonAnimals);
        Button buttonZoos = new Button("Zoos", event -> loadZooView());
        horizontalLayout.add(buttonZoos);
        //Load home view
        loadHomeView();
    }

    //Functions

    //Load Menu and title
    private void loadMenuWithTitle(String titleInsert){
        titleLabel.setText(titleInsert);
        add(titleLabel);
        add(horizontalLayout);
    }

    //Load Welcome view
    private void loadHomeView() {
        removeAll();
        loadMenuWithTitle("Welcome to our Zoo app");
    }

    //Load Caretaker view
    private void loadCaretakerView(){
        removeAll();
        loadMenuWithTitle("Caretaker listing");

        Grid<AnimalCaretaker> grid = new Grid<>(AnimalCaretaker.class);

        // Initialize filter field on name
        TextField filter = new TextField();
        filter.setPlaceholder("Filter by firstname");
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listCaretakersFilteredByNameStartsWithIgnoreCase(e.getValue(),grid));
        // Initialize button for new entry
        Button addNewBtn = new Button("New", VaadinIcon.PLUS.create());
        // Initialize editor
        AnimalCaretakerEditor editor = new AnimalCaretakerEditor(animalCaretakerRepository);
        // Build layout
        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        // Add to view
        add(actions, grid, editor);
        //Test print to view the keys of the columns
        for (Grid.Column c : grid.getColumns()){
            System.out.println(c.getKey());
        }

        // Only show the name and age columns
        grid.setColumns("firstname", "lastname", "age");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);

        // Connect selected AnimalCaretaker to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> {
            editor.editAnimalCaretaker(e.getValue());
        });

        // Instantiate and edit new AnimalCaretaker the new button is clicked
        addNewBtn.addClickListener(e -> editor.editAnimalCaretaker(new AnimalCaretaker("","",0)));

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listCaretakersFilteredByNameStartsWithIgnoreCase(filter.getValue(),grid);
        });

        // Fill in grid with all data
        listAnimalCaretaker(grid);

        // Click on row displays dialog with details (products)
        /*grid.asSingleSelect().addValueChangeListener(
                item -> {
                    Dialog dialog = new Dialog();
                    Label dialogLabel = new Label("Products sold by " + item.getValue().getName() + " (staff: " + item.getValue().getStaffnumber() + ")");
                    dialog.add(dialogLabel);
                    Grid<Product> detailGrid = new Grid<>(Product.class);
                    detailGrid.setItems(productRepository.findByFoodtruckId(item.getValue().getId()));
                    detailGrid.removeColumnByKey("id");
                    detailGrid.removeColumnByKey("foodtruck");
                    detailGrid.setHeightByRows(true);
                    detailGrid.setWidth("260px");
                    dialog.add(detailGrid);
                    dialog.open();
                });*/


        // Button in grid that opens a detail dialog
        /*grid.addColumn(new NativeButtonRenderer<>("View products",
                item -> {
                    Dialog dialog = new Dialog();
                    Grid<Product> detailGrid = new Grid<>(Product.class);
                    detailGrid.setItems(productRepository.findByFoodtruckId(item.getId()));
                    detailGrid.removeColumnByKey("id");
                    detailGrid.removeColumnByKey("foodtruck");
                    detailGrid.setHeightByRows(true);
                    detailGrid.setWidth("260px");
                    dialog.add(detailGrid);
                    dialog.open();
                }));*/
    }

    //Load all Caretakers into grid
    private void listAnimalCaretaker(Grid<AnimalCaretaker> grid) {
        grid.setItems(animalCaretakerRepository.findAll());
    }
    //Load caretakers into grid based on a name filter (starts with & ignore case), if empty load all
    private void listCaretakersFilteredByNameStartsWithIgnoreCase(String filterText, Grid<AnimalCaretaker> grid) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(animalCaretakerRepository.findAll());
        }
        else {
            grid.setItems(animalCaretakerRepository.findByFirstnameStartsWithIgnoreCase(filterText));
        }
    }


    //Load Animal view
    private void loadAnimalView(){
        removeAll();
        loadMenuWithTitle("Animal listing");

        Grid<Animal> grid = new Grid<>(Animal.class);

        // Initialize filter field on name
        TextField filter = new TextField();
        filter.setPlaceholder("Filter by species");
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listAnimalsFilteredByNameStartsWithIgnoreCase(e.getValue(),grid));
        // Initialize button for new entry
        Button addNewBtn = new Button("New", VaadinIcon.PLUS.create());
        // Initialize editor
        AnimalEditor editor = new AnimalEditor(animalRepository);
        // Build layout
        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        // Add to view
        add(actions, grid, editor);
        //Test print to view the keys of the columns
        for (Grid.Column c : grid.getColumns()){
            System.out.println(c.getKey());
        }

        // Only show species,family & gender collumns
        grid.setColumns("species", "family","gender");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);

        // Connect selected animal to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> {
            editor.editAnimal(e.getValue());
        });

        // Instantiate and edit new Animal the new button is clicked
        addNewBtn.addClickListener(e -> editor.editAnimal(new Animal("",null,"")));

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listAnimalsFilteredByNameStartsWithIgnoreCase(filter.getValue(),grid);
        });

        // Fill in grid with all data
        listAnimals(grid);

        /*
        // Button in grid that opens a detail dialog
        grid.addColumn(new NativeButtonRenderer<>("View participating foodtrucks",
                item -> {
                    Dialog dialog = new Dialog();
                    Grid<Foodtruck> detailGrid = new Grid<>(Foodtruck.class);
                    detailGrid.setItems(item.getFoodtrucks());
                    // Remove id, events and products columns, i.e. only show the name and staffnumber columns
                    detailGrid.setColumns("name", "staffnumber");
                    detailGrid.setHeightByRows(true);
                    detailGrid.setWidth("260px");
                    dialog.add(detailGrid);
                    dialog.open();
                }));*/
    }

    //Load all Animals into grid
    private void listAnimals(Grid<Animal> grid) {
        grid.setItems(animalRepository.findAll());
    }
    //Load Animals into grid based on a name filter (starts with & ignore case), if empty load all
    private void listAnimalsFilteredByNameStartsWithIgnoreCase(String filterText, Grid<Animal> grid) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(animalRepository.findAll());
        }
        else {
            grid.setItems(animalRepository.findBySpeciesStartsWithIgnoreCase(filterText));
        }
    }

    //Load Zoo view
    private void loadZooView(){
        removeAll();
        loadMenuWithTitle("Zoo listing");

        Grid<Zoo> grid = new Grid<>(Zoo.class);

        // Initialize filter field on name
        TextField filter = new TextField();
        filter.setPlaceholder("Filter by name");
        filter.setValueChangeMode(ValueChangeMode.EAGER);
        filter.addValueChangeListener(e -> listZoosFilteredByNameStartsWithIgnoreCase(e.getValue(),grid));
        // Initialize button for new entry
        Button addNewBtn = new Button("New", VaadinIcon.PLUS.create());
        // Initialize editor
        ZooEditor editor = new ZooEditor(zooRepository);
        // Build layout
        HorizontalLayout actions = new HorizontalLayout(filter, addNewBtn);
        // Add to view
        add(actions, grid, editor);
        //Test print to view the keys of the columns
        for (Grid.Column c : grid.getColumns()){
            System.out.println(c.getKey());
        }

        // Only show the name,location and share columns
        grid.setColumns("name", "location", "entree");
        grid.setSelectionMode(Grid.SelectionMode.SINGLE);

        // Connect selected Zoo to editor or hide if none is selected
        grid.asSingleSelect().addValueChangeListener(e -> {
            editor.editZoo(e.getValue());
        });

        // Instantiate and edit new Zoos the new button is clicked
        addNewBtn.addClickListener(e -> editor.editZoo(new Zoo("","",0)));

        // Listen changes made by the editor, refresh data from backend
        editor.setChangeHandler(() -> {
            editor.setVisible(false);
            listZoosFilteredByNameStartsWithIgnoreCase(filter.getValue(),grid);
        });

        // Fill in grid with all data
        listZoo(grid);

        // Click on row displays dialog with details (products)
        /*grid.asSingleSelect().addValueChangeListener(
                item -> {
                    Dialog dialog = new Dialog();
                    Label dialogLabel = new Label("Products sold by " + item.getValue().getName() + " (staff: " + item.getValue().getStaffnumber() + ")");
                    dialog.add(dialogLabel);
                    Grid<Product> detailGrid = new Grid<>(Product.class);
                    detailGrid.setItems(productRepository.findByFoodtruckId(item.getValue().getId()));
                    detailGrid.removeColumnByKey("id");
                    detailGrid.removeColumnByKey("foodtruck");
                    detailGrid.setHeightByRows(true);
                    detailGrid.setWidth("260px");
                    dialog.add(detailGrid);
                    dialog.open();
                });*/


        // Button in grid that opens a detail dialog
        /*grid.addColumn(new NativeButtonRenderer<>("View products",
                item -> {
                    Dialog dialog = new Dialog();
                    Grid<Product> detailGrid = new Grid<>(Product.class);
                    detailGrid.setItems(productRepository.findByFoodtruckId(item.getId()));
                    detailGrid.removeColumnByKey("id");
                    detailGrid.removeColumnByKey("foodtruck");
                    detailGrid.setHeightByRows(true);
                    detailGrid.setWidth("260px");
                    dialog.add(detailGrid);
                    dialog.open();
                }));*/
    }

    //Load all Zoos into grid
    private void listZoo(Grid<Zoo> grid) {
        grid.setItems(zooRepository.findAll());
    }
    //Load caretakers into grid based on a name filter (starts with & ignore case), if empty load all
    private void listZoosFilteredByNameStartsWithIgnoreCase(String filterText, Grid<Zoo> grid) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(zooRepository.findAll());
        }
        else {
            grid.setItems(zooRepository.findByNameStartsWithIgnoreCase(filterText));
        }
    }


}
