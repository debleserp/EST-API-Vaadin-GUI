package com.debleserp.demo.webvaadin.components;


import com.debleserp.demo.entity.Animal;
import com.debleserp.demo.repository.AnimalRepository;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class AnimalEditor extends VerticalLayout implements KeyNotifier {

    private final AnimalRepository repository;

    /**
     * The currently edited entity
     */
    private Animal animal;

    /* Fields to edit properties in entity */
    TextField speciesField = new TextField("Species");
    //ADD VALUES COMBOBOX ENUM TYPE
    TextField familyField = new TextField("Family");
    TextField genderField = new TextField("Gender");

    /* Action buttons */
    Button save = new Button("Save", VaadinIcon.CHECK.create());
    Button cancel = new Button("Cancel");
    Button delete = new Button("Delete", VaadinIcon.TRASH.create());
    HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

    Binder<Animal> binder = new Binder<>(Animal.class);
    private ChangeHandler changeHandler;

    @Autowired
    public AnimalEditor(AnimalRepository repository) {
        this.repository = repository;

        //COMBOBOX ERROR!!
        add(speciesField,genderField,familyField, actions);

        // Bind using naming convention
        binder.bind(speciesField, Animal::getSpecies, Animal::setSpecies);
        binder.bind(genderField, Animal::getGender, Animal::setGender);

        // Configure and style components
        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());

        // wire action buttons to save, delete and reset
        save.addClickListener(e -> save());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> editAnimal(animal));
        setVisible(false);
    }

    void delete() {
        repository.delete(animal);
        changeHandler.onChange();
    }

    void save() {
        repository.save(animal);
        changeHandler.onChange();
    }

    public interface ChangeHandler {
        void onChange();
    }

    public final void editAnimal(Animal c) {
        if (c == null) {
            setVisible(false);
            return;
        }
        final boolean persisted = c.getId() != null;
        if (persisted) {
            // Find fresh entity for editing
            animal = repository.findById(c.getId()).get();
        }
        else {
            animal = c;
        }
        cancel.setVisible(persisted);

        // Bind zoo properties to similarly named fields
        // Could also use annotation or "manual binding" or programmatically
        // moving values from fields to entities before saving
        binder.setBean(animal);

        setVisible(true);

        // Focus name initially
        speciesField.focus();
    }

    public void setChangeHandler(ChangeHandler h) {
        // ChangeHandler is notified when either save or delete
        // is clicked
        changeHandler = h;
    }
}