package com.debleserp.demo.webvaadin.components;


import com.debleserp.demo.entity.Zoo;
import com.debleserp.demo.repository.ZooRepository;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.KeyNotifier;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.converter.StringToDoubleConverter;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.springframework.beans.factory.annotation.Autowired;

@SpringComponent
@UIScope
public class ZooEditor extends VerticalLayout implements KeyNotifier {

    private final ZooRepository repository;

    /**
     * The currently edited entity
     */
    private Zoo zoo;

    /* Fields to edit properties in entity */
    TextField nameField = new TextField("Name");
    TextField locationField = new TextField("Location");
    TextField entreeField = new TextField("Entree");

    /* Action buttons */
    Button save = new Button("Save", VaadinIcon.CHECK.create());
    Button cancel = new Button("Cancel");
    Button delete = new Button("Delete", VaadinIcon.TRASH.create());
    HorizontalLayout actions = new HorizontalLayout(save, cancel, delete);

    Binder<Zoo> binder = new Binder<>(Zoo.class);
    private ChangeHandler changeHandler;

    @Autowired
    public ZooEditor(ZooRepository repository) {
        this.repository = repository;

        add(nameField, locationField, entreeField, actions);

        // Bind using naming convention
        binder.bind(nameField, Zoo::getName, Zoo::setName);
        binder.bind(locationField, Zoo::getLocation, Zoo::setLocation);
        binder.forField(entreeField)
                .withConverter(new StringToDoubleConverter("Must enter a number"))
                .bind(Zoo::getEntree, Zoo::setEntree);

        // Configure and style components
        setSpacing(true);

        save.getElement().getThemeList().add("primary");
        delete.getElement().getThemeList().add("error");

        addKeyPressListener(Key.ENTER, e -> save());

        // wire action buttons to save, delete and reset
        save.addClickListener(e -> save());
        delete.addClickListener(e -> delete());
        cancel.addClickListener(e -> editZoo(zoo));
        setVisible(false);
    }

    void delete() {
        repository.delete(zoo);
        changeHandler.onChange();
    }

    void save() {
        repository.save(zoo);
        changeHandler.onChange();
    }

    public interface ChangeHandler {
        void onChange();
    }

    public final void editZoo(Zoo c) {
        if (c == null) {
            setVisible(false);
            return;
        }
        final boolean persisted = c.getId() != null;
        if (persisted) {
            // Find fresh entity for editing
            zoo = repository.findById(c.getId()).get();
        }
        else {
            zoo = c;
        }
        cancel.setVisible(persisted);

        // Bind zoo properties to similarly named fields
        // Could also use annotation or "manual binding" or programmatically
        // moving values from fields to entities before saving
        binder.setBean(zoo);

        setVisible(true);

        // Focus name initially
        nameField.focus();
    }

    public void setChangeHandler(ChangeHandler h) {
        // ChangeHandler is notified when either save or delete
        // is clicked
        changeHandler = h;
    }
}