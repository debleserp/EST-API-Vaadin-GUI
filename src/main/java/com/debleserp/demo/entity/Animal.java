package com.debleserp.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "animals")
public class Animal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "animal_id")
    private Long id;

    @NotNull
    private String species;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Family family;

    @NotNull
    private String gender;

    @NotNull
    @ManyToMany (cascade = CascadeType.ALL)
    @JoinTable(name = "caretakers_animals",
            joinColumns = @JoinColumn(name = "animal_id", referencedColumnName = "animal_id",nullable = false),
            inverseJoinColumns = @JoinColumn(name = "caretaker_id", referencedColumnName = "caretaker_id",nullable = false))
    private Set<AnimalCaretaker> caretakers = new HashSet<AnimalCaretaker>();

    @NotNull
    @ManyToMany (cascade = CascadeType.ALL)
    @JsonBackReference
    @JoinTable(name = "animals_zoos",
            joinColumns = @JoinColumn(name = "animal_id", referencedColumnName = "animal_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "zoo_id", referencedColumnName = "zoo_id", nullable = false))
    private Set<Zoo> zoos = new HashSet<Zoo>();

    public Animal() {
    }

    public Animal(@NotNull String species, @NotNull Family family, @NotNull String gender) {
        this.species = species;
        this.family = family;
        this.gender = gender;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Set<AnimalCaretaker> getCaretakers() {
        return caretakers;
    }

    public void setCaretakers(Set<AnimalCaretaker> caretakers) {
        this.caretakers = caretakers;
    }

    public Set<Zoo> getZoos() {
        return zoos;
    }

    public void setZoos(Set<Zoo> zoos) {
        this.zoos = zoos;
    }


    public void addCaretaker(AnimalCaretaker caretaker){
        caretaker.getAnimals().add(this);
        caretakers.add(caretaker);
    }

    public void removeCaretaker(AnimalCaretaker caretaker){
        caretakers.remove(caretaker);
        caretaker.getAnimals().remove(this);
    }

    public void addZoo(Zoo zoo){
        zoo.getAnimals().add(this);
        zoos.add(zoo);
    }

    public void removeZoo(Zoo zoo){
        zoos.remove(zoo);
        zoo.getAnimals().remove(this);
    }
}
