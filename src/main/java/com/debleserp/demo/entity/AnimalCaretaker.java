package com.debleserp.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "caretakers")
public class AnimalCaretaker {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    @Column(name = "caretaker_id")
    private Long id;

    @NotNull
    @Size(max = 30)
    private String firstname;

    @NotNull
    private String lastname;

    @NotNull
    private int age;

    @NotNull
    @ManyToMany (fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    @JsonBackReference
    @JoinTable(name = "caretakers_animals",
        joinColumns = @JoinColumn(name = "caretaker_id", referencedColumnName = "caretaker_id"),
        inverseJoinColumns = @JoinColumn(name = "animal_id", referencedColumnName = "animal_id"))
    private Set<Animal> animals = new HashSet<Animal>();


    public AnimalCaretaker() {
    }

    public AnimalCaretaker(@NotNull @Size(max = 30) String firstname, @NotNull String lastname, @NotNull int age) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Set<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(Set<Animal> animals) {
        this.animals = animals;
    }

    public void addAnimal(Animal animal){
        animal.getCaretakers().add(this);
        animals.add(animal);
    }

    public void removeAnimal(Animal animal){
        animals.remove(animal);
        animal.getCaretakers().remove(this);
    }
}
