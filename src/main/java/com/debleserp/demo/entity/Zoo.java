package com.debleserp.demo.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "zoos")
public class Zoo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "zoo_id")
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String location;

    private double entree;

    @NotNull
    @ManyToMany (cascade = CascadeType.ALL)
    @JoinTable(name = "animals_zoos",
            joinColumns = @JoinColumn(name = "zoo_id", referencedColumnName = "zoo_id",nullable = true),
            inverseJoinColumns = @JoinColumn(name = "animal_id", referencedColumnName = "animal_id",nullable = true))
    private Set<Animal> animals = new HashSet<Animal>();


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "zoo", cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<Owner> owners = new HashSet<Owner>();

    public Zoo() {
    }

    public Zoo(@NotNull String name, @NotNull String location, double entree) {
        this.name = name;
        this.location = location;
        this.entree = entree;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public double getEntree() {
        return entree;
    }

    public void setEntree(double entree) {
        this.entree = entree;
    }

    public Set<Animal> getAnimals() {
        return animals;
    }

    public void setAnimals(Set<Animal> animals) {
        this.animals = animals;
    }

    public Set<Owner> getOwners() {
        return owners;
    }

    public void setOwners(Set<Owner> owners) {
        this.owners = owners;
    }

    public void addOwner(Owner owner){
        owner.setZoo(this);
        owners.add(owner);
    }

    public void removeOwner(Owner owner){
        owner.setZoo(null);
        owners.remove(owner);
    }

    public void addAnimal(Animal animal){
        animal.getZoos().add(this);
        animals.add(animal);
    }

    public void removeAnimal(Animal animal){
        animals.remove(animal);
        animal.getZoos().remove(this);
    }
}
