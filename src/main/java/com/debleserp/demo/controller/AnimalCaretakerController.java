    package com.debleserp.demo.controller;


import com.debleserp.demo.entity.AnimalCaretaker;
import com.debleserp.demo.exception.ResourceNotFoundException;
import com.debleserp.demo.repository.AnimalCaretakerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
public class AnimalCaretakerController {

    @Autowired
    private AnimalCaretakerRepository animalCaretakerRepository;

    // Get all caretakers
    @GetMapping("/caretakers")
    public Collection<AnimalCaretaker> getAllCaretakers() {
        return animalCaretakerRepository.findAll();
    }

    // Create a new caretaker
    @PostMapping("/caretakers")
    public AnimalCaretaker createCaretaker(@Valid @RequestBody AnimalCaretaker animalCaretaker){
        return animalCaretakerRepository.save(animalCaretaker);
    }

    // Get a single caretaker by id
    @GetMapping("/caretakers/{id}")
    public AnimalCaretaker getCaretakersbyId(@PathVariable Long id){
        return animalCaretakerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Caretaker", "id", id));
    }

    // Update an caretaker
    @PutMapping ("/caretakers/{id}")
    public AnimalCaretaker updateAnimalCaretaker(@Valid @RequestBody AnimalCaretaker animalCaretakerDetails, @PathVariable long id) {
        AnimalCaretaker animalCaretaker = animalCaretakerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Caretaker", "id", id));

        animalCaretaker.setFirstname(animalCaretakerDetails.getFirstname());
        animalCaretaker.setLastname(animalCaretakerDetails.getLastname());
        animalCaretaker.setAge(animalCaretakerDetails.getAge());

        return animalCaretakerRepository.save(animalCaretaker);
    }

    // Delete a caretaker
    @DeleteMapping ("/caretakers/{id}")
    public ResponseEntity<?> deleteAnimalCaretaker (@PathVariable long id) {
        AnimalCaretaker caretaker = animalCaretakerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("AnimalCaretaker", "id", id));
        animalCaretakerRepository.delete(caretaker);

        return ResponseEntity.ok().build();
    }
}
