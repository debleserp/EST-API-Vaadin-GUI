package com.debleserp.demo.controller;


import com.debleserp.demo.entity.Owner;
import com.debleserp.demo.exception.ResourceNotFoundException;
import com.debleserp.demo.repository.OwnerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
public class OwnerController {

    @Autowired
    private OwnerRepository ownerRepository;

    // Get all owners
    @GetMapping("/owners")
    public Collection<Owner> getAllOwners() {
        return ownerRepository.findAll();
    }

    // Create a new owner
    @PostMapping("/owners")
    public Owner createEvent(@Valid @RequestBody Owner owner){
        return ownerRepository.save(owner);
    }

    // Get a single owner by id
    @GetMapping("/owners/{id}")
    public Owner getOwnersbyId(@PathVariable Long id){
        return ownerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Owner", "id", id));
    }

    // Update an owner
    @PutMapping ("/owners/{id}")
    public Owner updateOwner(@Valid @RequestBody Owner ownerDetails, @PathVariable long id) {
        Owner owner = ownerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Owner", "id", id));

        owner.setFirstname(ownerDetails.getFirstname());
        owner.setLastname(ownerDetails.getLastname());
        owner.setShare(ownerDetails.getShare());
        owner.setZoo(ownerDetails.getZoo());

        return ownerRepository.save(owner);
    }

    // Delete an owner
    @DeleteMapping ("/owners/{id}")
    public ResponseEntity<?> deleteOwner (@PathVariable long id) {
        Owner owner = ownerRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Owner", "id", id));
        ownerRepository.delete(owner);

        return ResponseEntity.ok().build();
    }
}