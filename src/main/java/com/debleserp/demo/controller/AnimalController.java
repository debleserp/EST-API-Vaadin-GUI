package com.debleserp.demo.controller;


import com.debleserp.demo.entity.Animal;
import com.debleserp.demo.exception.ResourceNotFoundException;
import com.debleserp.demo.repository.AnimalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
public class AnimalController {

    @Autowired
    private AnimalRepository animalRepository;

    // Get all animal
    @GetMapping("/animals")
    public Collection<Animal> getAllAnimals() {
        return animalRepository.findAll();
    }

    // Create a new animal
    @PostMapping("/animals")
    public Animal createEvent(@Valid @RequestBody Animal animal){
        return animalRepository.save(animal);
    }

    // Get a single animal by id
    @GetMapping("/animals/{id}")
    public Animal getAnimalbyId(@PathVariable Long id){
        return animalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Animal", "id", id));
    }

    // Update an animal
    @PutMapping ("/animals/{id}")
    public Animal updateAnimal(@Valid @RequestBody Animal animalDetails, @PathVariable long id) {
        Animal animal = animalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Animal", "id", id));

        animal.setSpecies(animalDetails.getSpecies());
        animal.setFamily(animalDetails.getFamily());
        animal.setGender(animalDetails.getGender());
        animal.setCaretakers(animalDetails.getCaretakers());

        return animalRepository.save(animal);
    }

    // Delete an animal
    @DeleteMapping ("/animals/{id}")
    public ResponseEntity<?> deleteAnimal (@PathVariable long id) {
        Animal animal = animalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Animal", "id", id));
        animalRepository.delete(animal);

        return ResponseEntity.ok().build();
    }
}
