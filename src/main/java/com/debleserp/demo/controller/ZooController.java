package com.debleserp.demo.controller;

import com.debleserp.demo.entity.Animal;
import com.debleserp.demo.entity.Owner;
import com.debleserp.demo.entity.Zoo;
import com.debleserp.demo.exception.ResourceNotFoundException;
import com.debleserp.demo.repository.ZooRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
public class ZooController {

    @Autowired
    private ZooRepository zooRepository;

    // Get all zoos
    @GetMapping("/zoos")
    public Collection<Zoo> getAllZoos(){
        return zooRepository.findAll();
    }

    //Get a single zoo by id
    @GetMapping("/zoos/{id}")
    public Zoo getZoobyId(@PathVariable Long id){
        return zooRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Zoo", "id", id));
    }

    // Get a single zoo's owner by id
    @GetMapping("/zoos/{id}/owners")
    public Collection<Owner> getZooOwners(@PathVariable Long id) {
        Zoo zoo = zooRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Zoo", "id", id));

        return zoo.getOwners();
    }

    // Get a single zoo's animals by id
    @GetMapping("/zoos/{id}/animals")
    public Collection<Animal> getZooAnimals(@PathVariable Long id) {
        Zoo zoo = zooRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Zoo", "id", id));

        return zoo.getAnimals();
    }

    // Create a new zoo
    @PostMapping("/zoos")
    public Zoo createZoo(@Valid @RequestBody Zoo zoo){
        return zooRepository.save(zoo);
    }

    // Delete a zoo
    @DeleteMapping("/zoos/{id}")
    public ResponseEntity<?> deleteZoo(@PathVariable Long id) {
        Zoo zoo = zooRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Zoo", "id", id));

        zooRepository.delete(zoo);

        return ResponseEntity.ok().build();
    }

    //Update an event
    @PutMapping("/zoos/{id}")
    public Zoo updateZoo(@Valid @RequestBody Zoo zooDetails, @PathVariable Long id) {

        Zoo zoo = zooRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Zoo", "id", id));

        zoo.setName(zooDetails.getName());
        zoo.setLocation(zooDetails.getLocation());
        zoo.setEntree(zooDetails.getEntree());
        zoo.setOwners(zooDetails.getOwners());
        zoo.setAnimals(zooDetails.getAnimals());

        return zooRepository.save(zoo);
    }

}