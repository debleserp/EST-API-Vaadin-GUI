package com.debleserp.demo.repository;


import com.debleserp.demo.entity.Zoo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ZooRepository extends JpaRepository<Zoo, Long> {
    List<Zoo> findByNameStartsWithIgnoreCase(String name);
}
