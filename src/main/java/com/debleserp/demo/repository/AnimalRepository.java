package com.debleserp.demo.repository;

import com.debleserp.demo.entity.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimalRepository extends JpaRepository<Animal, Long> {
    List<Animal> findBySpeciesStartsWithIgnoreCase(String species);

}
