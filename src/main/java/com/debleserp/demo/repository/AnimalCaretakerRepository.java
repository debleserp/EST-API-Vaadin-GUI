package com.debleserp.demo.repository;

import com.debleserp.demo.entity.AnimalCaretaker;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnimalCaretakerRepository extends JpaRepository<AnimalCaretaker, Long> {
    List<AnimalCaretaker> findByFirstnameStartsWithIgnoreCase(String firstName);
}
