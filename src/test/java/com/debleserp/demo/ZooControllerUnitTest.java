package com.debleserp.demo;

import com.debleserp.demo.controller.ZooController;
import com.debleserp.demo.entity.Animal;
import com.debleserp.demo.entity.AnimalCaretaker;
import com.debleserp.demo.entity.Owner;
import com.debleserp.demo.entity.Zoo;
import com.debleserp.demo.repository.ZooRepository;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ZooController.class)
public class ZooControllerUnitTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ZooRepository zooRepository;

    private ObjectMapper mapper = new ObjectMapper();

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    @Test
    public void givenZoos_whenGetAllZoos_thenReturnJsonArray()
            throws Exception {

        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        List<Zoo> allZoos = Arrays.asList(dierentuin);

        given(zooRepository.findAll()).willReturn(allZoos);

        mvc.perform(get("/zoos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name").value(dierentuin.getName()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal1.getSpecies()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal2.getSpecies()));
    }


    @Test
    public void givenZoo_whenGetZooById_thenReturnZoo()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        given(zooRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(dierentuin));

        mvc.perform(get("/zoos/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(dierentuin.getName()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal1.getSpecies()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal2.getSpecies()));
    }

    @Test
    public void givenZoo_whenGetZooOwners_thenReturnJsonArray()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        given(zooRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(dierentuin));

        mvc.perform(get("/zoos/1/owners")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].firstName").value("Test1"))
                .andExpect(jsonPath("$[0].lastName").value("Test2"));
    }

    @Test
    public void givenZoo_whenGetZooEntree_thenReturnString()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);


        given(zooRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(dierentuin));

        mvc.perform(get("/zoos/{id}/entree", 1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("Your entree is €" + dierentuin.getEntree()));
    }

    @Test
    public void whenCreateZoo_thenStatus200()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        mvc.perform(post("/zoos")
                .content(convertObjectToJsonBytes(dierentuin))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void givenFoodtruck_whenUpdateFoodtruck_thenStatus200()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        Zoo editdierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("editTest, editLocation 576");
        dierentuin.setEntree(25);

        Animal editanimal1 = new Animal();
        animal1.setSpecies("edittest");
        animal1.setGender("Female");

        Animal editanimal2 = new Animal();
        animal2.setSpecies("edittest2");
        animal2.setGender("Male");

        AnimalCaretaker editcaretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Edittester");
        caretaker1.setLastname("von Edittest");
        caretaker1.setAge(25);

        AnimalCaretaker editcaretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Edittest");
        caretaker2.setLastname("Editjanssens");
        caretaker2.setAge(54);

        Owner editowner1 = new Owner();
        owner1.setFirstname("Editbert");
        owner1.setLastname("Edittester");
        owner1.setShare(69);

        Owner editowner2 = new Owner();
        owner2.setFirstname("Editdirk");
        owner2.setLastname("van den Editdierentuin");
        owner2.setShare(31);

        animal1.addCaretaker(editcaretaker1);
        animal1.addCaretaker(editcaretaker2);
        animal2.addCaretaker(editcaretaker1);
        animal2.addCaretaker(editcaretaker2);
        dierentuin.addAnimal(editanimal1);
        dierentuin.addAnimal(editanimal2);
        dierentuin.addOwner(editowner1);
        dierentuin.addOwner(editowner2);

        given(zooRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(dierentuin));

        mvc.perform(put("/zoos/{id}",1L)
                .content(convertObjectToJsonBytes(editdierentuin))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void whenDeleteFoodtruck_thenStatus200()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        given(zooRepository.findById(1L)).willReturn(java.util.Optional.ofNullable(dierentuin));

        mvc.perform(delete("/zoos/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }




}
