package com.debleserp.demo;

import com.debleserp.demo.entity.Animal;
import com.debleserp.demo.entity.AnimalCaretaker;
import com.debleserp.demo.entity.Owner;
import com.debleserp.demo.entity.Zoo;
import com.debleserp.demo.repository.ZooRepository;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class ZooControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ZooRepository zooRepository;

    private ObjectMapper mapper = new ObjectMapper();

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    @Test
    public void givenZoos_whenGetAllZoos_thenReturnJsonArray()
            throws Exception {

        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        zooRepository.save(dierentuin);

        mvc.perform(get("/zoos")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name").value(dierentuin.getName()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal1.getSpecies()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal2.getSpecies()));
    }


    @Test
    public void givenFoodtruck_whenGetFoodtruckById_thenReturnFoodtruck()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        zooRepository.save(dierentuin);

        mvc.perform(get("/zoos/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value(dierentuin.getName()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal1.getSpecies()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal2.getSpecies()));

        mvc.perform(get("/zoos/{id}",100001L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Zoo Antwerpen"));
    }

    @Test
    public void givenZoo_whenGetZooOwners_thenReturnJsonArray()
            throws Exception {

        mvc.perform(get("/zoos/200001/owners")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].firstName").value("Test1"))
                .andExpect(jsonPath("$[0].lastName").value("Test2"));

    }

    @Test
    public void givenZoo_whenGetZooEntree_thenReturnString()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        zooRepository.save(dierentuin);

        mvc.perform(get("/zoos/{id}/entree", 1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("Your entree is €" + dierentuin.getEntree()));
    }

    @Test
    public void whenCreateZoo_thenReturnZoo()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        zooRepository.save(dierentuin);

        mvc.perform(post("/zoos")
                .content(convertObjectToJsonBytes(dierentuin))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(dierentuin.getName()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal1.getSpecies()))
                .andExpect(jsonPath("$[0].animals[0].species").value(animal2.getSpecies()))
                .andExpect(jsonPath("$[0].owners[0].firstName").value("Test1"))
                .andExpect(jsonPath("$[0].owners[0].lastName").value("Test2"));

        Zoo foundZoo = zooRepository.findById(1L).get();
        assertThat(foundZoo.getName()).isEqualTo(dierentuin.getName());
    }

    @Test
    public void givenZoo_whenUpdateZoo_thenReturnZoo()
            throws Exception {

        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        zooRepository.save(dierentuin);

        dierentuin.setName("Editzoo");
        dierentuin.setEntree(30);

        mvc.perform(put("/zoos/{id}",dierentuin.getId())
                .content(convertObjectToJsonBytes(dierentuin))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("Editzoo"));

        Zoo foundZoo = zooRepository.findById(dierentuin.getId()).get();
        assertThat(foundZoo.getName()).isEqualTo("Edittruck");
    }

    @Test
    public void whenDeleteZoo_thenStatus200()
            throws Exception {
        Zoo dierentuin = new Zoo();
        dierentuin.setName("TestDierentuin");
        dierentuin.setLocation("Test, Location 576");
        dierentuin.setEntree(25);

        Animal animal1 = new Animal();
        animal1.setSpecies("test");
        animal1.setGender("Male");

        Animal animal2 = new Animal();
        animal2.setSpecies("test2");
        animal2.setGender("Female");

        AnimalCaretaker caretaker1 = new AnimalCaretaker();
        caretaker1.setFirstname("Tester");
        caretaker1.setLastname("von Test");
        caretaker1.setAge(30);

        AnimalCaretaker caretaker2 = new AnimalCaretaker();
        caretaker2.setFirstname("Test");
        caretaker2.setLastname("Janssens");
        caretaker2.setAge(49);

        Owner owner1 = new Owner();
        owner1.setFirstname("Bert");
        owner1.setLastname("Tester");
        owner1.setShare(50);

        Owner owner2 = new Owner();
        owner2.setFirstname("Dirk");
        owner2.setLastname("van den Dierentuin");
        owner2.setShare(50);

        animal1.addCaretaker(caretaker1);
        animal1.addCaretaker(caretaker2);
        animal2.addCaretaker(caretaker1);
        animal2.addCaretaker(caretaker2);
        dierentuin.addAnimal(animal1);
        dierentuin.addAnimal(animal2);
        dierentuin.addOwner(owner1);
        dierentuin.addOwner(owner2);

        zooRepository.save(dierentuin);

        mvc.perform(delete("/zoos/{id}",1L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        assertThat(zooRepository.findById(1L).isPresent()).isEqualTo(false);
    }

}
